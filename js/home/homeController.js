
var HomeController = function HomeController($scope, $http, $stateParams, apiService, $state) {
        
        function init(){
            
            //get teams
            apiService.getTeams().then(function(teams){
                
                //pull the values we care about
                $scope.teams = _.map(teams, function(team){ 
                    return {
                        FullName:team.FullName,
                        Key:team.Key
                    }; 
                });;
                
                //first run through
                runQuery(teams[0].Key);
            });
            
        }
              
        function runQuery(team) {
            if(!team){
                team = $('#team').find(":selected").val();
            }
            
            apiService.getPlayerStatsByTeam($('#year').find(":selected").val(), team).then(function(data){
                
                //get stat types
                if($scope.statTypes.length === 0){
                    for(var name in data[0]) {
                        if(!isNaN(data[0][name]) && name != "PlayerID"){
                            $scope.statTypes.push(name);
                        }
                    }
                }
                else{
                    //$scope.selectedStat = $('#statType').find(":selected").val();
                }
                
                var sorted = _.sortBy(data, $scope.selectedStat).reverse();
                
                sorted = _.map(sorted, function(player){ 
                    return [player.Name, player[$scope.selectedStat]]; 
                });
               
               // Create the data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('number', $scope.selectedStat);
                data.addRows(sorted);

                // Set chart options
                var options = {'title':'Team ' + $scope.selectedStat,
                            'width':800,
                            'height':600};

                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            });
        }

        $scope.runQuery = runQuery;
        $scope.teams = [];
        $scope.statTypes = [];
        $scope.selectedStat = 'Receptions';

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(init);
}

HomeController.$inject = ['$scope', '$http', '$stateParams', 'apiService', '$state'];
TrailMapper.controller('HomeController', HomeController);