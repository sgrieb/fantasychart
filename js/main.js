/**
 * Created by Steve on 11/24/2015.
 */
var TrailMapper = angular.module('trailmapper', ['ui.router', 'ngCookies']);

TrailMapper.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    //
    // For any unmatched url, redirect to /home
    $urlRouterProvider.otherwise("/");

    $locationProvider.html5Mode(true);
    
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "js/home/home.html",
            controller: 'HomeController',
            data:{
                 authenticate:true
            }
        })
        .state('login', {
            url: "/login",
            templateUrl: "js/login/login.html",
            controller: 'LoginController',
            data:{
                 authenticate:false
            }
        })
        .state('login.auth', {
            url: "/{accessToken}",
            templateUrl: "js/login/login.html",
            controller: 'LoginController',
            data:{
                 authenticate:false
            }
        })
})
.run(['$log', '$rootScope', '$cookies', '$state', '$http', 'configService', '$q', 'apiService', function ($log, $rootScope, $cookies, $state, $http, configService, $q, apiService) {
    
    
            // $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
            //     if(toState.data.authenticate && $cookies.get('trailmapper_ua') == null){
            //         event.preventDefault();
            //         $state.go('login');
            //     }
            //     else{
            //         configService.accessToken = $cookies.get('trailmapper_ua');
            //     }
            // });
    
            $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").toggleClass("toggled");
            });
}]);