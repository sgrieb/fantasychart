var apiService = function ($http, configService, $q) {
    return {
        getPlayerStatsByTeam: getPlayerStatsByTeam,
        getTeams:getTeams
    };
    
    function getTeams() {
        return $http({
                method: 'GET',
                headers: configService.header,
                url: configService.baseUrl + "/scores/JSON/Teams"
            })
            .then(getTeamsComplete)
            .catch(getTeamsFailed);

        function getTeamsComplete(response) {
            return response.data;
        }

        function getTeamsFailed(error) {
            alert('error');
        }
    }
    
    function getPlayerStatsByTeam(year, team) {
        return $http({
                method: 'GET',
                headers: configService.header,
                url: configService.baseUrl + "/stats/JSON/PlayerSeasonStatsByTeam/"+ year + "/" + team
            })
            .then(getPlayerStatsByTeamComplete)
            .catch(getPlayerStatsByTeamFailed);

        function getPlayerStatsByTeamComplete(response) {
            return response.data;
        }

        function getPlayerStatsByTeamFailed(error) {
            alert('error');
        }
    }
}
apiService.$inject = ['$http', 'configService', '$q'];
TrailMapper.service('apiService', apiService);