### What is this? ###

Many people use apps like Strava or the UnderArmour Suite(MapMyHike, MapMyRun, MapMyRide) to track their fitness activity and look at where they have been on an interactive map via those apps.  However, one feature that I have noticed none of these services provide is the ability to overlay ALL of the "routes" you have ever taken on map.

This could be useful for things like: Planning future routes, identifying highly frequented routes, overlaying additional map data (eg: trailheads, stoplights, etc)

Also, maybe I like Strava for running, but MapMyHike for hiking!  Wouldn't it be great if I could combine that data and see my running+hiking coverage?  

### Features ###

* Authentication with UnderArmour API via OAuth2 (Angular + Python Backend)
* Gets All Routes and Overlays them on a simple OpenStreet Map (ESRI Javascript API)
* Zoom to extent of all routes submitted by User
* Map + Sidebar using Bootstrap Responsive Design (works on mobile)

### Future Features? (in no particular order) ###

* Integrate another 3rd party provider (Strava)
* Enhanced Map interactions + Route details
* Route Editing (I accidentally forgot to end my workout then drove 5 miles!)
* Route Planning/Creation
* Location Coverage + Road Networks ("You have covered 65% of the trails in XXX State Park")
* Hosting

### Hosting ###
This app is not publicly hosted yet.  Eventually it will be in Amazon, but for now the $0 budget for the project must be maintained.

The devops side of this project is VERY rough.  Setting server names, config values and such will need to be greatly improved to make the move to Amazon.

The backend needed to support the OAuth2 is simply a python script that needs to be run on the server.

While this isn't hosted 24/7, I have a local setup on my home network + public facing IP + port forwarding + RaspberryPi that can be used to demo a working prototype (on request).  This is simply using apache and the python script.

### Setup ###

IMPORTANT: THE AUTH FOR THIS APP WILL NOT WORK FOR YOU, THIS IS ONLY CODE

There are many config values and such that are still hardcoded in (localhost etc) so that is just ONE reason why this won't work.  The other more important reason is dictated by how OAuth2 works.  The UnderArmour API has been set to only accept redirects to a specific server, and you are not that server :)

Development:

npm install

bower install

grunt serve

To package:

grunt