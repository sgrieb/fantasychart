/**
 * Created by Steve on 11/2/2015.
 */
module.exports = function(grunt) {

    var url = require("url"),
    fs = require("fs");
    
    // The default file if the file/path is not found
    var defaultFile = "index.html"

    // I had to resolve to the previous folder, because this task lives inside a ./tasks folder
    // If that's not your case, just use `__dirname`
    var folder = __dirname;

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            dist: {
                files: [
                    // includes files within path
                    {expand: true, src: ['fonts/*', 'favicon/*', 'css/*', 'partials/*', 'index.html', 'img/*', 'lib/*', 'js/**/*.html'], dest: 'dist/'},
                ],
            }
        },
        concat: {
            dist: {
                //order matters
                src: ['js/*.js', 'js/**/*.js'],
                dest: 'dist/js/trailmapper.js'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: [{
                    expand: true,
                    src: 'dist/js/trailmapper.js'
                }]
            }
        },
        processhtml: {
            dist: {
                files: [{
                    expand: true,
                    src: 'dist/index.html'
                }]
            },
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html'
                    ]
                },
                options: {
                    server: {
                        baseDir: "./",
                        middleware: function(req, res, next) {
                            var fileName = url.parse(req.url);
                            fileName = fileName.href.split(fileName.search).join("");
                            var fileExists = fs.existsSync(folder + fileName);
                            if (!fileExists && fileName.indexOf("browser-sync-client") < 0) {
                                req.url = "/" + defaultFile;
                            }
                            return next();
                        }
                    }
                }
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-processhtml');
    
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    grunt.loadNpmTasks('grunt-contrib-concat');
    
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('serve', ['browserSync']);

    grunt.registerTask('default', ['copy', 'concat', 'uglify', 'processhtml']);

};