### What is this? ###

This is a Javascript app that will use OAuth to allow you to log into your "MapMyFitness" account and visualize all of the routes you have taken on an interactive ArcGIS map

### Benefits 
* I just want to be able to see all of the places I've hiked!

### Hosting ###
Not hosted yet

### Setup ###

Development:

npm install

bower install

grunt serve

To package:

grunt